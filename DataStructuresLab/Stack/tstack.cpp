// ����, ���, ���� "������ ����������������-2, C++, ���
//
// Copyright (�) ������� �.�. 27.07.2000 (07.08)
//
// ������������ ��������� ������ - ���� - ������ 3.1

#include <cstdio>
#include "tstack.h"
#include <windows.h>

/* �������� � ���� */
void Stack::Put(const TData &Val) 
{ 
	if (pMem == nullptr) 
		SetRetCode(DataNoMem);	// SKIP_ON
	else if (IsFull()) 
		SetRetCode(DataFull);
	else 
	{
		LastElementIdx = GetNextIndex(LastElementIdx);
		pMem[LastElementIdx] = Val;
		DataCount++;
	}
} // Put

/* ����� �� ����� � ���������  */
TData Stack::Get(void) 
{ 
	TData temp = -1;
	if (pMem == NULL) 
		SetRetCode(DataNoMem);
	else if (IsEmpty()) 
		SetRetCode(DataEmpty);
	else 
	{
		temp = pMem[LastElementIdx--];
		DataCount--;
	}
	return temp;
}

/* �������� ��������� �������� ������� */
int Stack::GetNextIndex(int index) 
{ 
	return ++index;
} 

void gotoxy(int xpos, int ypos)
{
	COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X = xpos; scrn.Y = ypos;
	SetConsoleCursorPosition(hOuput, scrn);
}

/* �������� ������� ��������� */
void Stack::Paint(int y, int x1, int x2)
{ 
	int i, px;
	gotoxy(x1, y);
	if (DataCount == 0) 
		px = x1 - 1;
	else 
	{
		px = x1 + double(DataCount) * (x2 - x1 + 1) / MemSize - 1;
		if ((px < x1) && (DataCount > 0)) 
			px - x1;
		if (DataCount == MemSize) 
			px = x2;
	}
	for (i = x1; i <= px; i++) 
		printf("#");
	for (i = px + 1; i <= x2; i++) 
		printf(" ");
	if ((x1 < 1) || (x1 > 79) || (px < 1) || (px > 79) || (x2 < 1) || (x2 > 79)) 
	{
		printf("Error in parameters of painting...\n");
		getchar();
	}
}

/* ������ �������� ����a */
void Stack::Print() 
{ 
	for (int i = 0; i < DataCount; i++)
		printf("%d ", pMem[i]);
	printf("\n");
	getchar();
}

/* ������������ ��������� */
int Stack::IsValid() 
{ 
	int res = 0;
	if (pMem == NULL)	        
		res = 1;
	if (MemSize < DataCount)	
		res += 2;
	return res;
}

/* ���������� � ������� */
void Stack::CopyToVector(TElem v[]) 
{
	
	for (int i = 0; i < DataCount; i++)
		v[i] = pMem[i];
}
