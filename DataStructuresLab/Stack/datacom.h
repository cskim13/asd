// ����, ���, ���� "������ ����������������-2", C++, ���
//
// datacom.h - Copyright(�) ������� �.�. 30.08.2000
//
// ��������� ����� ����������

#ifndef 	__DATACOM_H
#define 	__DATACOM_H

#define DataOK 0 
#define DataErr -1

/* Notes:
*   DataCom �������� ����� ������� �������
*/
class DataCom
{
protected:
	int RetCode; // ��� ����������
	int SetRetCode(int ret) { return RetCode = ret; };
public:
	DataCom() : RetCode(DataOK) {};
	virtual ~DataCom() {};
	int GetRetCode()
	{
		int temp = RetCode; 
		RetCode = DataOK; 
		return temp;
	};
};

#endif
