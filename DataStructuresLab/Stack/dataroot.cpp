// ����, ���, ���� "������ ����������������-2", C++, ���
//
// dataroot.cpp - Copyright (�) ������� �.�. 28.07.2000 (06.08)
//
// ������������ ��������� ������ - ������� (�����������) ����� - ������ 3.1 // ������ ���������� ����������� ��� �������� ������� SetMem

#include <cstdio>
#include "dataroot.h"

DataRoot::DataRoot(int Size) : DataCom() 
{
	DataCount = 0;
	MemSize = Size;
	if (Size == 0)
	{
		// ������ ����� ����������� ������� SetMem
		pMem = nullptr;
		MemType = MEM_RENTER;
	}
	else 
	{ 
		// ������ ���������� ��������
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
	}
}

/* ���������� */
DataRoot :: ~DataRoot() 
{
	if (MemType == MEM_HOLDER) 
		delete pMem;
	pMem = nullptr;
}

/* ������� ������ */
void DataRoot::SetMem(void *pMem, int Size) 
{ 
	
	if (MemType == MEM_HOLDER) 
		delete pMem; // ! ���������� �� �����������
	pMem = (TElem *)pMem;
	MemType = MEM_RENTER;
	MemSize = Size;
} 

/* �������� ������� �� */
int DataRoot::IsEmpty(void) const 
{ 
	return DataCount == 0;
}

/* �������� ������������ �� */
int DataRoot::IsFull(void) const 
{  
	return DataCount == MemSize;
}
