// ����, ���, ���� "������ ����������������-?", C++, ���
//
// Copyright (�) ������� �.�. 28.07.2000 (07.08)
//
// ������������ ��������� ������ - ����

#ifndef __DATSTACK_H
#define __DATSTACK_H

#include "dataroot.h"

#define StackID 101

class Stack : public DataRoot
{
protected:
	/* ������ ���������� �������� ��������� */
	int LastElementIdx;     
	/* �������� ��������� ������  */
	virtual int GetNextIndex(int index);   
public:
	/* ������ ������ ���� */
	Stack(int Size = DefaultMemSize) : DataRoot(Size) { LastElementIdx = -1; }
	
	/* �������� � ���� (#�1)  */
	virtual void Put(const TData &Vai);    
	
	/* ����� �� ����� � ��������� */
	virtual TData Get(void);	     

	virtual void Print();// ������ ��������
protected:
	// ��������� ������
	
	// �������� ������� ��������� 
	virtual void Paint(int �, int x1, int x2); 
	
	// ������������ ���������
	virtual int IsValid(); 
	
	// ���������� � �������
	virtual void CopyToVector(TElem v[]);  
};
#endif




