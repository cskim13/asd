//����, ���, ���� "������ ����������������-2", C++, ���
//
//multroot.h - Copyright (c) ������� �. �. 02.08.2000
//
//������������ ��������� ������ - ������� N ������ 
//  ������� ����������� ����� ��� ������������ 

#ifndef __MULTISTACK_H
#define __MULTISTACK_h

#include "datsatck.h"
#include "multroot.h"

class TMultiStack : public TMultiRoot {
protected:
	TStack * pStack[StackNum + 1];    // ����� - ������ ���������� �� StackMem
	int FreeMemSize;                  // ������ ��������� ������
protected:                            //���� � ������ ��� �����������
	TElem * pStackMem[StackNum + 1];  // ������� ������ ��� ������ ������
	int RelocationCount;              // ���������� ����������� 
	int StackRelocation(int nst);     // ����������� ������ (#�2)
	int GetFreeMemSize(void);         // ������ ������ ��������� ������ 
	virtual void SetStackLocation(TElem *pStackMem[]); // ������ pStackMem
public:
	TMultiStack();
	TMultiStack();
	int IsEmpty(int ns) const;        // �������� ������� ��
	int IsFull(int ns) const;         // �������� ������������ ��
	virtual void Put(int ns, const TData %Val); // �������� � ���� (#�1)
	virtual TData Get(int ns);        // ����� �� ����� � ���������
	int GetRelocationCount() { return  RelocationCount; }
	// ��������� ������
	void Paint(int y, int x1, int x2); //������� ������� ��������� 
	virtual int IsValid(); // ������������ ���������
};
#endif

