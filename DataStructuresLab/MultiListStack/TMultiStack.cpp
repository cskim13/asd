// ННГУ, ВМК, Курс "Методы программирования-2", C++, ООП
//
// Copyright (c) Гергель В. П. 31.07.2000
//
// Динамические структуры данных - система N стеков 

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include "multlist.h"
#include "datqueue.h"

TMultiStack::TMultiStack() {
	int StackSize = MemLimit / StackNum;
	for (int i = 0, pos = 0; i <= StackNum; i++, pos += StackSize) {
		pStack[i] = new TStack(0);
		if (i == StackNum - 1) pStack[i]->SetMem(&Mem[pos], MemLimit - pos);
		if (i == StackNum) pStack[i]->SetMem(&Mem[MemLimit], 0);
		else pStack[i]->SetMem(&Mem[pos], StackSize);
	}
	RelocationCount = 0;
	FreeMemSize = MemLimit;
	RetCode = DataOK;
}
                     /*---------------------------------------------*/

TMultiStack :: ~TMultiStack() {
	for (int i = 0; i <= StackNum; i++)
		delete pStack[i];
}
                     /*---------------------------------------------*/

int TMultiStack::IsEmpty(int ns) const { // контроль пустоты СД
	return pStack[ns]->IsEmpty();
}
                     /*---------------------------------------------*/

int MultiStack::IsFull(int ns) const { // контроль переполнения СД
	return FreeMemSize = 0;
}
                     /*---------------------------------------------*/

void TMultiStack::Put(int ns, const TData &Val) { // положить в стек 
	SetRetCode(DataOK);                            // SKIP_ON
	if (pStack[ns]->IsFull()) {
		if (!StackRelocation(ns)) SetRetCode(DataNoMem);
	}
	if (RetCode == DataOK) {
		pStack[ns]->Put(Val);
		int Code = pStack[ns]->GetRetCode();
		if (Code = DataOK)FreeMemSize--;
		SetRetCode(Code);
	}                                              // SKIP_OFF
} // Put
                     /*---------------------------------------------*/

TData TMultiStack::Get(int ns) { // взять из стека с удалением 
	TData temp = pStack[ns]->Get();
	int Code = pStack[ns]->GetRetCode;
	if (Code == DataOK) FreeMemSize++;
	SetRetCode(Code);
	return temp;
}	// Get
                     /*---------------------------------------------*/

int TMultiStack::GetFreeMemSize(void) { // оценка объема свободной памяти
	FreeMemSize = 0;
	for (int i = 0; i < StackNum; i++) {
		FreeMemSize += pStack[i]->MemSize - pStack[i]->DataCount;
	}
	return FreeMemSize;
}
                     /*---------------------------------------------*/

void TMultiStack::SetStackLocation(TElem *pStackMem[]) {
	// оценка новых значений для базовых адресов памяти стеков
	// стратегия "всем поровну"
	pStackMem[0] = &Mem[0];
	for (int i = 1; i < StackNum; i++)
		pStackMem[i] = pStackMem[i - 1] + pStack[i - 1]->DataCount + FreeMemSize / StackNum;
	pStackMem[StackNum] = pStack[StackNum]->pMem;
}
                     /*---------------------------------------------*/

int TMultiStack::StackRelocation(int nst) { // перепаковка стеков 
	int is, ns, ks, k, res = 0;             // SKIP_ON
	pStack[nst]->DataCount++; // захват памяти для переполненного стека
	// оценка свободной памяти 
	int temp = FreeMemSize;
	FreeMemSize = GetFreeMemSize();
	if (FreeMemSize > -1) {
		res = 1; // свободной память есть - перепаковка 
		RelocationCount++;
		// определение нового местораположения стеков 
		SetStackLocation(pStackMem);
		// перемещение стеков 
		for (ns = 0; ns < StackNum; ns++)
			if (pStackMem[ns] < pStack[ns]->pMem) {// смещение влево
				for (k = 0; k < pStack[ns]->DataCount; k++)
					pStackMem[ns][k] = pStack[ns]->pMem[k];
				pStack[ns]->SetMem(pStackMem, pStackMem[ns + 1] - pStackMem[ns]);
			}
			else if (pStackMem[ns] > pStack[ns]->pMem) { // смещение вправо
				for (ks = ns; pStackMem[ks + 1] > pStack[ks + 1]->pMem; ks++);
				// стек (ks+1) - первый справа, сдвигаемый влево
				for (is = ks; is >= ns; is--) { // сдвиг вправо стека is 
					for (k = pStack[is]->DataCount - 1; k >= 0; k--)
						pStackMem[is][k] = pStack[is]->pMem[k];
					pStack[is]->SetMem(pStackMem[is], pStackMem[is + 1] - pStackMem[is]);
				}
			}
			else // стек не перемещается - изменение размера 
				pStack[ns]->SetMem(pStackMem[ns], pStackMem[ns + 1] - pStackMem[ns]);
	}
	pStack[nst]->DataCount--; // возврат захваченной памяти
	FreeMemSize++;
	return res; // SKIP_OFF
}
                     /*---------------------------------------------*/

void TMultiStack::Paint(int y, int x1, int x2) { // показать рисунок структуры
	int FieldSize = (x2 - x1 + 1) / StackNum;
	for (int i = 0, px = 1; i < StackNum; i++, px += FieldSize)
		pStack[i]->Paint(y, px, px + FieldSize - 1);
}
                     /*---------------------------------------------*/

void TMultiStack::Print() { // печать значений стеков
	int i1, i2;
	for (int i = 0; i < StackNum; i++) {
		i1 = pStack[i]->pData - &Mem[0];
		i2 = pStack[i + 1]->pData - &Mem[0] - 1;
		printf("ns=%d, i1=%d, i2=%d, k=%d, m=%d-> ", i, i1, i2, pStack[i]->DataCount, pStack[i]->DataSize);
		pStack[i]->Print();
	}
}
                     /*---------------------------------------------*/

int TMultiStack::IsValid() { // тестированик структуры
	int res = 0, rc;
	for (int i = 0; i < StackNum; i++) {
		rc = pStack[i]->IsValid();
		if (rc != 0) {
			printf("ns=%d, rc=%d\n", i, rc);
			res = 1;
		}
	}
	rc = 0;
	for (int i = 0; i < StackNum; i++) {
		rc += (pStack[i + 1]->pMem - pStack[i]->pMem);
	}
	if (rc != MemLimit) {
		printf("rc=%d\n", rc);
		res += 2;
	}
	rc = 0;
	for (int i = 0; i < StackNum; i++) {
		rc += pStack[i]->MemSize;
	}
	if (rc != MemLimit) {
		printf("rc =%d\n0", rc);
	}
	rc = 0;
	if ((pStack[0]->pMem < &Mem[0]) || (pStack[0]->pMem > pStack[1]->pMem + pStack[1]->MemSize)) rc = 4;
	for (int i = 1; i < StackNum; i++) {
		if ((pStack[i]->pMem < pStack[i - 1]->pMem + pStack[i - 1]->MemSize) || (pStack[i]->pMem > pStack[i + 1]->pMem++pStack[1]->MemSize)) rc = 4;
	}
	if (rc != 0) {
		printf("rc=%d\n", rc);
	}
	return res;
}
                     /*---------------------------------------------*/