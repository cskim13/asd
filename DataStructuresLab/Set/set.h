#ifndef __SET_H
#define __SET_H

#include "bitfield.h"

class TSet
{
private:
	int MaxPower; //������������ �������� ���������
	TBitField BitField;
public:
	TSet(int mp); 
	TSet(const TSet &s); //����������� �����������
	TSet(const TBitField &bf); //����������� �������������� ����
	//������ � �����
	int GetMaxPower(void)const; //������������ �������� ���������
	void IntElem(const int n); //�������� ������� � ���������
	void DelElem(const int n); //������� ������� �� ���������
	int IsMember(const int n) const; //��������� ������� �������� � ���������
	//���������-������������� ��������
	int operator==(const TSet &s); //���������
	TSet & operator=(cosnt TSet &s); //������������
	TSet operator+(const int n); //��������� �������� � ���������
	TSet operator-(const int n); //�������� �������� �� ���������
	TSet operator+(const TSet &s); //����������� 
	TSet operator*(const TSet &s); //�����������
	TSet operator~(void); //����������
	friend istream &operator>>(istream &istr, TSet &bf);
	friend ostream &operator<<(ostream &ostr, const TSet &bf);
};
#endif