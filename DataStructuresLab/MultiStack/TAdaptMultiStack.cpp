// ����, ���, ���� "������ ����������������-2", C++, ���
//
// Copyright (c) ������� �. �. 01.08.2000
//
// ������������ ��������� ������ - ������� N ������ 
// ���������� ����� ������� ���������� 
#include <time.h>
#include "multist3.h"
void TAdaptMultiStack::SetStackLocation(TElem *pStackMem[]) {
	// ������� ����� ���������
	long NewTime;
	int NewInc;
	TComplexMultiStack::SetStackLocation(pStackMem);
	NewTime = time(&NewTime);
	if (NewTime > PrevTIme + TimeStep) { // ���������
		NewInc = RelocationCount - PrevCount;
		if (NewInc > PrevInc) {
			MemQuota -= QuotaStep; if (MemQuota < 0) MemQuota = 0.0;
		}
		else if (NewInc < PrevInc) {
			MemQuota += QuotaStep; if (MemQuota > 1) MemQuota = 1.0;
		}
		PrevTime = NewTime;
		PrevInc = NewInc;
		PrevCount = RelocationCount;
	}
}