/ ����, ���, ���� "������ ����������������-2", C++, ���
//
// Copyright (c) ������� �. �. 01.08.2000
//
// ������������ ��������� ������ - ������� N ������ 
// ������������� ������ �� ��������������� �����

#ifndef __MULTISTACK2_H
#define __MULTISTACK2_H

#include "multist1.h"

class TComplexMultiStack : public TSuperMultiStack {
protected:
	double MemQuota; // ���� ������, �������������� �� ��������� �����
public:
	TComplexMultiStack() { MemQuota = 0.0; }
	double GetMemQuota(void) const { return MemQuota; }
	void SetMemQuota(double val) { MemQuota = val; }
	void SetStackLocation(TElem *pStackMem[]); // ������ pStackMem
};
#endif
// ����, ���, ���� "������ ����������������-2", C++, ���
//
// Copyright (c) ������� �. �. 01.08.2000
//
// ������������ ��������� ������ - ������� N ������ 
// ������������� ������ �� ��������������� �����
