// ����, ���, ���� "������ ����������������-2", C++, ���
//
// Copyright (c) ������� �. �. 01.08.2000
//
// ������������ ��������� ������ - ������� N ������ 
// ������������ - ������������� ������ �� ��������
//  " ���������� ��������� ��������� �����"

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include "multist1.h"

TSuperMultiStack::TSuperMultiStack() {
	for (int i = 0; i < StackNum; i++) {
		PrevCount[i] = 0; StackRate[i] = 0;
	}
}                     /*------------------------------------------------*/
void TSuperMultiStack::SetStackRate(double StackRate[]) {
	// ������ ����������� ����� ������ 
	double SumRate = 0.0;
	for (int i = 0; i < StackNum; i++) {
		if (pStack[i]->DataCount < PrevCount[i])StackRate[i] = 0;
		else StackRate[i] = pStack[i]->DataCount - PrevCount[i];
		SumRate += StaackRate[i];
	}
	if (SumRate > 0)
		for (int i = 0; i < StackNum; i++) StackRate[i] /= SumRate;
}                     /*------------------------------------------------*/
void TSuperMultiStack::SetStackLocation(TElem *pStackMem[]) {
	// ������ ����� �������� ��� ������� ������� ������ ������ 
	// ��������� "���������� ��������� ��������� �����"
	SetStackRate(StackRate);
	pStackMem[0] = &Mem[0];
	for (int i = 1; i < StackNum; i++)
		pStackMem[i] = pStackMem[i - 1] + pStack[i - 1]->DataCount + int(StackRate[i - 1] * FreeMemSize);
	pStackMem[StackNum] = pStack[StackNum]->pMem;
	for (int i = 0; i < StackNum; i++)
		PrevCount[i] = pStack[i]->DataCount;
}
