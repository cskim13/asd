// ����, ���, ���� "������ ����������������-2", C++, ���
//
// Copyright (c) ������� �. �. 01.08.2000
//
// ������������ ��������� ������ - ������� N ������ 
// ������������� ������ �� ��������������� �����

#include <stdio.h>
#include <conio.h>
#include <string.h>
#include "multist2.h"

void TComplexMultiStack::SetStackLocation(TElem *pStackMem[]) {
	// ������ ����� �������� ��� ������� ������� ������ ������ SKIP_ON
	// ��������� ��������� 
	SetStackRate(StackRate);
	pStackMem[0] = &Mem[0];
	for (int i = 1; i < StackNum; i++)
		pStackMem[i] = pStackMem[i - 1] + pStack[i - 1]->DataCount+int(MemQuota*FreeMemSize/StackNum) // ������� 
		+ int(1 - MemQuota)*StackRate[i - 1] * FreeMemSize); // ���� �����
		pStackMem[StackNum] = pStack[StackNum]->pMem;
		for (int i = 0; i < StackNum; i++)
			PrevCount[i] = pStack[i]->DataCount; // SKIP_OFF
}