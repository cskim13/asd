// ����, ���, ���� "������ ����������������-2", C++, ���
//
// Copyright (c) ������� �. �. 01.08.2000
//
// ������������ ��������� ������ - ������� N ������ 
// ���������� ����� ������� ���������� 

#ifndef __MULTISTACK3_H
#define __MULTISTACK3_H

#include "multist2.h"
\
class TAdaptMultiStack :public TComplexMultiStack {
protected:
	double QuotaStep;  // ��� ��������� ���� ������
	long TimeStep;     // �������� ������� ��� �������� �����������
	long PrevTime;     // ����� ����������� ���� ���������
	int PrevCount;     // �-�� ����������� �� ���������� ����
	int PrevInc;       // �������� �-�� ����������� �� ���������� ����
public:
	TAdaptMultiStack() {
		QuotaStep = 0.1; TimeStep = 2;
		PrevTIme = PrevCount = PrevInc = 0;
	}
	void SetQuotaStep(double step) { Quota = step; }
	void SetTimeStep(int step) { TimeStep = step; }
	void SetStackLocation(TElem *pStackMem[]); // ������ pStackMem
};
#endif